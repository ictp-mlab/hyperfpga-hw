# This constraint file was automatically generated 
 #based on the pinout Excel File. It is only valid for 4EG. 

#----- North Connector -----
# Carrier signal name B228_RX0_N
#set_property PACKAGE_PIN Y1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B228_RX0_P
#set_property PACKAGE_PIN Y2 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B228_TX0_N
#set_property PACKAGE_PIN W3 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B228_TX0_P
#set_property PACKAGE_PIN W4 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B229_RX0_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B229_RX0_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B229_TX0_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B229_TX0_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B230_RX0_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B230_RX0_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B230_TX0_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B230_TX0_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B128_RX0_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B128_RX0_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B128_TX0_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B128_TX0_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L14_N
#set_property PACKAGE_PIN D5 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L14_P
#set_property PACKAGE_PIN E5 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L1_N
#set_property PACKAGE_PIN E4 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L1_P
#set_property PACKAGE_PIN E3 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L15_N
#set_property PACKAGE_PIN G6 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L15_P
#set_property PACKAGE_PIN F6 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L8_N
#set_property PACKAGE_PIN G3 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L8_P
#set_property PACKAGE_PIN F3 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L18_N
#set_property PACKAGE_PIN F7 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L18_P
#set_property PACKAGE_PIN G8 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L10_N
#set_property PACKAGE_PIN F5 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L10_P
#set_property PACKAGE_PIN G5 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L8_N
#set_property PACKAGE_PIN D11 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L8_P
#set_property PACKAGE_PIN E12 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L9_N
#set_property PACKAGE_PIN G10 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L9_P
#set_property PACKAGE_PIN H11 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L24_N
#set_property PACKAGE_PIN H1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L24_P
#set_property PACKAGE_PIN J1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L17_N
#set_property PACKAGE_PIN J6 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L17_P
#set_property PACKAGE_PIN H6 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L22_N
#set_property PACKAGE_PIN L1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L22_P
#set_property PACKAGE_PIN K1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L21_N
#set_property PACKAGE_PIN J4 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L21_P
#set_property PACKAGE_PIN J5 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L20_N
#set_property PACKAGE_PIN N6 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L20_P
#set_property PACKAGE_PIN N7 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L23_N
#set_property PACKAGE_PIN AC1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L23_P
#set_property PACKAGE_PIN AB1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L24_N
#set_property PACKAGE_PIN AB4 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L24_P
#set_property PACKAGE_PIN AB3 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L22_N
#set_property PACKAGE_PIN AC2 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L22_P
#set_property PACKAGE_PIN AB2 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L23_N
#set_property PACKAGE_PIN AC1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L23_P
#set_property PACKAGE_PIN AB1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L19_N
#set_property PACKAGE_PIN AB6 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L19_P
#set_property PACKAGE_PIN AC6 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L15_N
#set_property PACKAGE_PIN AD2 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L15_P
#set_property PACKAGE_PIN AD1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L21_N
#set_property PACKAGE_PIN AF2 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L21_P
#set_property PACKAGE_PIN AE2 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L12_N
#set_property PACKAGE_PIN H12 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L12_P
#set_property PACKAGE_PIN J12 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L11_N
#set_property PACKAGE_PIN A11 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L11_P
#set_property PACKAGE_PIN A12 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L10_N
#set_property PACKAGE_PIN A10 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L10_P
#set_property PACKAGE_PIN B11 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L7_N
#set_property PACKAGE_PIN F11 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L7_P
#set_property PACKAGE_PIN F12 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]

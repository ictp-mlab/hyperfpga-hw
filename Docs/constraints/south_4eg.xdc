# This constraint file was automatically generated 
 #based on the pinout Excel File. It is only valid for 4EG. 

#----- South Connector -----
# Carrier signal name B228_TX2_N
#set_property PACKAGE_PIN R3 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B228_TX2_P
#set_property PACKAGE_PIN R4 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B228_RX2_N
#set_property PACKAGE_PIN T1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B228_RX2_P
#set_property PACKAGE_PIN T2 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B229_TX2_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B229_TX2_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B229_RX2_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B229_RX2_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B230_TX2_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B230_TX2_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B230_RX2_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B230_RX2_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B128_TX2_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B128_TX2_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B128_RX2_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B128_RX2_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L4_N
#set_property PACKAGE_PIN AF8 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L4_P
#set_property PACKAGE_PIN AG8 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L9_N
#set_property PACKAGE_PIN AH7 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L9_P
#set_property PACKAGE_PIN AH8 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L5_N
#set_property PACKAGE_PIN AC7 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L5_P
#set_property PACKAGE_PIN AB7 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L6_N
#set_property PACKAGE_PIN AG5 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L6_P
#set_property PACKAGE_PIN AG6 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L10_N
#set_property PACKAGE_PIN AH4 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L10_P
#set_property PACKAGE_PIN AG4 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L16_N
#set_property PACKAGE_PIN AH3 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L16_P
#set_property PACKAGE_PIN AG3 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L12_N
#set_property PACKAGE_PIN C13 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L12_P
#set_property PACKAGE_PIN C14 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L11_N
#set_property PACKAGE_PIN G14 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L11_P
#set_property PACKAGE_PIN G15 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L8_N
#set_property PACKAGE_PIN K7 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L8_P
#set_property PACKAGE_PIN K8 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L2_N
#set_property PACKAGE_PIN H8 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L2_P
#set_property PACKAGE_PIN H9 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L16_N
#set_property PACKAGE_PIN H3 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L16_P
#set_property PACKAGE_PIN H4 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L3_N
#set_property PACKAGE_PIN K9 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L3_P
#set_property PACKAGE_PIN J9 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L1_N
#set_property PACKAGE_PIN J7 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L1_P
#set_property PACKAGE_PIN H7 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L5_N
#set_property PACKAGE_PIN N8 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L5_P
#set_property PACKAGE_PIN N9 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L7_N
#set_property PACKAGE_PIN A1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L7_P
#set_property PACKAGE_PIN A2 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L8_N
#set_property PACKAGE_PIN G3 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L8_P
#set_property PACKAGE_PIN F3 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L1_N
#set_property PACKAGE_PIN E4 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L1_P
#set_property PACKAGE_PIN E3 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L2_N
#set_property PACKAGE_PIN A7 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L2_P
#set_property PACKAGE_PIN A6 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L12_N
#set_property PACKAGE_PIN D6 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L12_P
#set_property PACKAGE_PIN D7 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L3_N
#set_property PACKAGE_PIN B4 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L3_P
#set_property PACKAGE_PIN A4 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L7_N
#set_property PACKAGE_PIN E15 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L7_P
#set_property PACKAGE_PIN F15 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L9_N
#set_property PACKAGE_PIN L14 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L9_P
#set_property PACKAGE_PIN L13 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L10_N
#set_property PACKAGE_PIN H13 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L10_P
#set_property PACKAGE_PIN H14 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L8_N
#set_property PACKAGE_PIN D14 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L8_P
#set_property PACKAGE_PIN D15 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]

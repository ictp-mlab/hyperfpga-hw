# HyperFPGA Carrier Board Layout

The hyperFPGA carrier card host a SOM and provides several interfaces with other hyperFPGA carrier boards and other devices. Here is a list of the interfaces that are available with their corresponding pins:

| Resource   | Pins              | Interface             |
|------------|-------------------|-----------------------|
| QSPI       | MIO 0-12          | SOM SPI Flash         |
| PS GPIO 0  | MIO 0-25          |                       |
| PS GPIO 1  | MIO 26-51         |                       |
| PS GPIO 2  | MIO 52-77         |                       |
| PS SD 1    | MIO 46-51         | Carrier micro SD Card |
| PS CAN 1   | MIO 52-53         | Carrier CAN bus       |
| PS UART 0  | MIO 42-43         | Serial-USB converter  |
| PS UART 1  | MIO 56-57         | Serial-USB converter  |
| PS GEM 3   | MIO 64-75         | Gb Ethernet           |
| GPIO SW0-3 | MIO 19-22         | US0 switch            |
| GPIO SW4-7 | MIO 23-25, MIO 32 | US1 switch            |
| PCIe Reset | MIO 37            |                       |
| GPIO PB0   | MIO 34            | UB1                   |
| GPIO PB1   | MIO 33            | UB0                   |
| GPIO LED0  | MIO 35            | UL0                   |
| GPIO LED1  | MIO 36            | UL1                   |
| GPIO LED2  | MIO 44            | UL2                   |
| GPIO LED3  | MIO 62            | UL3                   |
| ETH_RST    | MIO 63            | ETH Phy reset         |
| PS I2C 0   | MIO 38-39         | SOM SI5338 addr. 0x70 |
| PLL RST    | MIO 58            | SI5338 reset          |
| PLL LOLn   | MIO 59            | SI5338 lock           |
| PLL SEL1   | MIO 60            | SI5338 sel1           |
| PLL SEL0   | MIO 61            | SI5338 sel0           |

## PMODS

There is a PMOD connector **P5** on the carrier board. It has 8 GPIO signals directly connected to the PL part of the SOC. This pins will depend on the SOM that is connected to the carrier, considering that the carrier is compatible with all Trenz Xilinx TE0803, Trenz Xilinx TE0808, and (theoretically) Sundance Microchip SoM1-MPF500T.

The table here shows only the pins for the TE0803-4GE21-L SOM.

|            | Pins              |
|------------|-------------------|
| PMOD 0 0   | K5                |
| PMOD 0 1   | P9                |
| PMOD 0 2   | H2                |
| PMOD 0 3   | W9                |
| PMOD 1 0   | AB5               |
| PMOD 1 1   | AE4               |
| PMOD 1 2   | AH6               |
| PMOD 1 3   | AD6               |

Net Status Report
Filename     : E:\gits\hyperfpga-carrier\Electronics\HyperFPGA-PCBA.PcbDoc
Date         : 10/20/2021
Time         : 4:34:03 PM
Time Elapsed : 00:00:00

Nets, Layer, Length
    +12V, Signal Layers Only, 0mm
    +3.3V_DC, Signal Layers Only, 0mm
    +3.3V_GT, Signal Layers Only, 0mm
    +3.3V_LP, Signal Layers Only, 0mm
    +3.3V_PL, Signal Layers Only, 0mm
    +3.3V_UTIL, Signal Layers Only, 0mm
    1.8V, Signal Layers Only, 0mm
    5V, Signal Layers Only, 0mm
    5V_SENSE_N, Signal Layers Only, 0mm
    5V_SENSE_P, Signal Layers Only, 0mm
    ADJ_SENSE_N, Signal Layers Only, 0mm
    ADJ_SENSE_P, Signal Layers Only, 0mm
    AVDD18, Signal Layers Only, 0mm
    AVDD3, Signal Layers Only, 0mm
    B128_RX0_N, Signal Layers Only, 0mm
    B128_RX0_P, Signal Layers Only, 0mm
    B128_RX1_N, Signal Layers Only, 0mm
    B128_RX1_P, Signal Layers Only, 0mm
    B128_RX2_N, Signal Layers Only, 0mm
    B128_RX2_P, Signal Layers Only, 0mm
    B128_RX3_N, Signal Layers Only, 0mm
    B128_RX3_P, Signal Layers Only, 0mm
    B128_TX0_N, Signal Layers Only, 0mm
    B128_TX0_P, Signal Layers Only, 0mm
    B128_TX1_N, Signal Layers Only, 0mm
    B128_TX1_P, Signal Layers Only, 0mm
    B128_TX2_N, Signal Layers Only, 0mm
    B128_TX2_P, Signal Layers Only, 0mm
    B128_TX3_N, Signal Layers Only, 0mm
    B128_TX3_P, Signal Layers Only, 0mm
    B228_RX0_N, Signal Layers Only, 0mm
    B228_RX0_P, Signal Layers Only, 0mm
    B228_RX1_N, Signal Layers Only, 0mm
    B228_RX1_P, Signal Layers Only, 0mm
    B228_RX2_N, Signal Layers Only, 0mm
    B228_RX2_P, Signal Layers Only, 0mm
    B228_RX3_N, Signal Layers Only, 0mm
    B228_RX3_P, Signal Layers Only, 0mm
    B228_TX0_N, Signal Layers Only, 0mm
    B228_TX0_P, Signal Layers Only, 0mm
    B228_TX1_N, Signal Layers Only, 0mm
    B228_TX1_P, Signal Layers Only, 0mm
    B228_TX2_N, Signal Layers Only, 0mm
    B228_TX2_P, Signal Layers Only, 0mm
    B228_TX3_N, Signal Layers Only, 0mm
    B228_TX3_P, Signal Layers Only, 0mm
    B229_RX0_N, Signal Layers Only, 0mm
    B229_RX0_P, Signal Layers Only, 0mm
    B229_RX1_N, Signal Layers Only, 0mm
    B229_RX1_P, Signal Layers Only, 0mm
    B229_RX2_N, Signal Layers Only, 0mm
    B229_RX2_P, Signal Layers Only, 0mm
    B229_RX3_N, Signal Layers Only, 0mm
    B229_RX3_P, Signal Layers Only, 0mm
    B229_TX0_N, Signal Layers Only, 0mm
    B229_TX0_P, Signal Layers Only, 0mm
    B229_TX1_N, Signal Layers Only, 0mm
    B229_TX1_P, Signal Layers Only, 0mm
    B229_TX2_N, Signal Layers Only, 0mm
    B229_TX2_P, Signal Layers Only, 0mm
    B229_TX3_N, Signal Layers Only, 0mm
    B229_TX3_P, Signal Layers Only, 0mm
    B230_RX0_N, Signal Layers Only, 0mm
    B230_RX0_P, Signal Layers Only, 0mm
    B230_RX1_N, Signal Layers Only, 0mm
    B230_RX1_P, Signal Layers Only, 0mm
    B230_RX2_N, Signal Layers Only, 0mm
    B230_RX2_P, Signal Layers Only, 0mm
    B230_RX3_N, Signal Layers Only, 0mm
    B230_RX3_P, Signal Layers Only, 0mm
    B230_TX0_N, Signal Layers Only, 0mm
    B230_TX0_P, Signal Layers Only, 0mm
    B230_TX1_N, Signal Layers Only, 0mm
    B230_TX1_P, Signal Layers Only, 0mm
    B230_TX2_N, Signal Layers Only, 0mm
    B230_TX2_P, Signal Layers Only, 0mm
    B230_TX3_N, Signal Layers Only, 0mm
    B230_TX3_P, Signal Layers Only, 0mm
    B47_L10_N, Signal Layers Only, 0mm
    B47_L10_P, Signal Layers Only, 0mm
    B47_L11_N, Signal Layers Only, 0mm
    B47_L11_P, Signal Layers Only, 0mm
    B47_L12_N, Signal Layers Only, 0mm
    B47_L12_P, Signal Layers Only, 0mm
    B47_L1_N, Signal Layers Only, 0mm
    B47_L1_P, Signal Layers Only, 0mm
    B47_L2_N, Signal Layers Only, 0mm
    B47_L2_P, Signal Layers Only, 0mm
    B47_L3_N, Signal Layers Only, 0mm
    B47_L3_P, Signal Layers Only, 0mm
    B47_L4_N, Signal Layers Only, 0mm
    B47_L4_P, Signal Layers Only, 0mm
    B47_L5_N, Signal Layers Only, 0mm
    B47_L5_P, Signal Layers Only, 0mm
    B47_L6_N, Signal Layers Only, 0mm
    B47_L6_P, Signal Layers Only, 0mm
    B47_L7_N, Signal Layers Only, 0mm
    B47_L7_P, Signal Layers Only, 0mm
    B47_L8_N, Signal Layers Only, 0mm
    B47_L8_P, Signal Layers Only, 0mm
    B47_L9_N, Signal Layers Only, 0mm
    B47_L9_P, Signal Layers Only, 0mm
    B48_L10_N, Signal Layers Only, 0mm
    B48_L10_P, Signal Layers Only, 0mm
    B48_L11_N, Signal Layers Only, 0mm
    B48_L11_P, Signal Layers Only, 0mm
    B48_L12_N, Signal Layers Only, 0mm
    B48_L12_P, Signal Layers Only, 0mm
    B48_L1_N, Signal Layers Only, 0mm
    B48_L1_P, Signal Layers Only, 0mm
    B48_L2_N, Signal Layers Only, 0mm
    B48_L2_P, Signal Layers Only, 0mm
    B48_L3_N, Signal Layers Only, 0mm
    B48_L3_P, Signal Layers Only, 0mm
    B48_L4_N, Signal Layers Only, 0mm
    B48_L4_P, Signal Layers Only, 0mm
    B48_L5_N, Signal Layers Only, 0mm
    B48_L5_P, Signal Layers Only, 0mm
    B48_L6_N, Signal Layers Only, 0mm
    B48_L6_P, Signal Layers Only, 0mm
    B48_L7_N, Signal Layers Only, 0mm
    B48_L7_P, Signal Layers Only, 0mm
    B48_L8_N, Signal Layers Only, 0mm
    B48_L8_P, Signal Layers Only, 0mm
    B48_L9_N, Signal Layers Only, 0mm
    B48_L9_P, Signal Layers Only, 0mm
    B64_L10_N, Signal Layers Only, 0mm
    B64_L10_P, Signal Layers Only, 0mm
    B64_L11_N, Signal Layers Only, 0mm
    B64_L11_P, Signal Layers Only, 0mm
    B64_L12_N, Signal Layers Only, 0mm
    B64_L12_P, Signal Layers Only, 0mm
    B64_L13_N, Signal Layers Only, 0mm
    B64_L13_P, Signal Layers Only, 0mm
    B64_L14_N, Signal Layers Only, 0mm
    B64_L14_P, Signal Layers Only, 0mm
    B64_L15_N, Signal Layers Only, 0mm
    B64_L15_P, Signal Layers Only, 0mm
    B64_L16_N, Signal Layers Only, 0mm
    B64_L16_P, Signal Layers Only, 0mm
    B64_L17_N, Signal Layers Only, 0mm
    B64_L17_P, Signal Layers Only, 0mm
    B64_L18_N, Signal Layers Only, 0mm
    B64_L18_P, Signal Layers Only, 0mm
    B64_L19_N, Signal Layers Only, 0mm
    B64_L19_P, Signal Layers Only, 0mm
    B64_L1_N, Signal Layers Only, 0mm
    B64_L1_P, Signal Layers Only, 0mm
    B64_L20_N, Signal Layers Only, 0mm
    B64_L20_P, Signal Layers Only, 0mm
    B64_L21_N, Signal Layers Only, 0mm
    B64_L21_P, Signal Layers Only, 0mm
    B64_L22_N, Signal Layers Only, 0mm
    B64_L22_P, Signal Layers Only, 0mm
    B64_L23_N, Signal Layers Only, 0mm
    B64_L23_P, Signal Layers Only, 0mm
    B64_L24_N, Signal Layers Only, 0mm
    B64_L24_P, Signal Layers Only, 0mm
    B64_L2_N, Signal Layers Only, 0mm
    B64_L2_P, Signal Layers Only, 0mm
    B64_L3_N, Signal Layers Only, 0mm
    B64_L3_P, Signal Layers Only, 0mm
    B64_L4_N, Signal Layers Only, 0mm
    B64_L4_P, Signal Layers Only, 0mm
    B64_L5_N, Signal Layers Only, 0mm
    B64_L5_P, Signal Layers Only, 0mm
    B64_L6_N, Signal Layers Only, 0mm
    B64_L6_P, Signal Layers Only, 0mm
    B64_L7_N, Signal Layers Only, 0mm
    B64_L7_P, Signal Layers Only, 0mm
    B64_L8_N, Signal Layers Only, 0mm
    B64_L8_P, Signal Layers Only, 0mm
    B64_L9_N, Signal Layers Only, 0mm
    B64_L9_P, Signal Layers Only, 0mm
    B64_T0, Signal Layers Only, 0mm
    B64_T1, Signal Layers Only, 0mm
    B64_T2, Signal Layers Only, 0mm
    B64_T3, Signal Layers Only, 0mm
    B65_L10_N, Signal Layers Only, 0mm
    B65_L10_P, Signal Layers Only, 0mm
    B65_L11_N, Signal Layers Only, 0mm
    B65_L11_P, Signal Layers Only, 0mm
    B65_L12_N, Signal Layers Only, 0mm
    B65_L12_P, Signal Layers Only, 0mm
    B65_L13_N, Signal Layers Only, 0mm
    B65_L13_P, Signal Layers Only, 0mm
    B65_L14_N, Signal Layers Only, 0mm
    B65_L14_P, Signal Layers Only, 0mm
    B65_L15_N, Signal Layers Only, 0mm
    B65_L15_P, Signal Layers Only, 0mm
    B65_L16_N, Signal Layers Only, 0mm
    B65_L16_P, Signal Layers Only, 0mm
    B65_L17_N, Signal Layers Only, 0mm
    B65_L17_P, Signal Layers Only, 0mm
    B65_L18_N, Signal Layers Only, 0mm
    B65_L18_P, Signal Layers Only, 0mm
    B65_L19_N, Signal Layers Only, 0mm
    B65_L19_P, Signal Layers Only, 0mm
    B65_L1_N, Signal Layers Only, 0mm
    B65_L1_P, Signal Layers Only, 0mm
    B65_L20_N, Signal Layers Only, 0mm
    B65_L20_P, Signal Layers Only, 0mm
    B65_L21_N, Signal Layers Only, 0mm
    B65_L21_P, Signal Layers Only, 0mm
    B65_L22_N, Signal Layers Only, 0mm
    B65_L22_P, Signal Layers Only, 0mm
    B65_L23_N, Signal Layers Only, 0mm
    B65_L23_P, Signal Layers Only, 0mm
    B65_L24_N, Signal Layers Only, 0mm
    B65_L24_P, Signal Layers Only, 0mm
    B65_L2_N, Signal Layers Only, 0mm
    B65_L2_P, Signal Layers Only, 0mm
    B65_L3_N, Signal Layers Only, 0mm
    B65_L3_P, Signal Layers Only, 0mm
    B65_L4_N, Signal Layers Only, 0mm
    B65_L4_P, Signal Layers Only, 0mm
    B65_L5_N, Signal Layers Only, 0mm
    B65_L5_P, Signal Layers Only, 0mm
    B65_L6_N, Signal Layers Only, 0mm
    B65_L6_P, Signal Layers Only, 0mm
    B65_L7_N, Signal Layers Only, 0mm
    B65_L7_P, Signal Layers Only, 0mm
    B65_L8_N, Signal Layers Only, 0mm
    B65_L8_P, Signal Layers Only, 0mm
    B65_L9_N, Signal Layers Only, 0mm
    B65_L9_P, Signal Layers Only, 0mm
    B65_T0, Signal Layers Only, 0mm
    B65_T1, Signal Layers Only, 0mm
    B65_T2, Signal Layers Only, 0mm
    B65_T3, Signal Layers Only, 0mm
    B66_L10_N, Signal Layers Only, 0mm
    B66_L10_P, Signal Layers Only, 0mm
    B66_L11_N, Signal Layers Only, 0mm
    B66_L11_P, Signal Layers Only, 0mm
    B66_L12_N, Signal Layers Only, 0mm
    B66_L12_P, Signal Layers Only, 0mm
    B66_L13_N, Signal Layers Only, 0mm
    B66_L13_P, Signal Layers Only, 0mm
    B66_L14_N, Signal Layers Only, 0mm
    B66_L14_P, Signal Layers Only, 0mm
    B66_L15_N, Signal Layers Only, 0mm
    B66_L15_P, Signal Layers Only, 0mm
    B66_L16_N, Signal Layers Only, 0mm
    B66_L16_P, Signal Layers Only, 0mm
    B66_L17_N, Signal Layers Only, 0mm
    B66_L17_P, Signal Layers Only, 0mm
    B66_L18_N, Signal Layers Only, 0mm
    B66_L18_P, Signal Layers Only, 0mm
    B66_L19_N, Signal Layers Only, 0mm
    B66_L19_P, Signal Layers Only, 0mm
    B66_L1_N, Signal Layers Only, 0mm
    B66_L1_P, Signal Layers Only, 0mm
    B66_L20_N, Signal Layers Only, 0mm
    B66_L20_P, Signal Layers Only, 0mm
    B66_L21_N, Signal Layers Only, 0mm
    B66_L21_P, Signal Layers Only, 0mm
    B66_L22_N, Signal Layers Only, 0mm
    B66_L22_P, Signal Layers Only, 0mm
    B66_L23_N, Signal Layers Only, 0mm
    B66_L23_P, Signal Layers Only, 0mm
    B66_L24_N, Signal Layers Only, 0mm
    B66_L24_P, Signal Layers Only, 0mm
    B66_L2_N, Signal Layers Only, 0mm
    B66_L2_P, Signal Layers Only, 0mm
    B66_L3_N, Signal Layers Only, 0mm
    B66_L3_P, Signal Layers Only, 0mm
    B66_L4_N, Signal Layers Only, 0mm
    B66_L4_P, Signal Layers Only, 0mm
    B66_L5_N, Signal Layers Only, 0mm
    B66_L5_P, Signal Layers Only, 0mm
    B66_L6_N, Signal Layers Only, 0mm
    B66_L6_P, Signal Layers Only, 0mm
    B66_L7_N, Signal Layers Only, 0mm
    B66_L7_P, Signal Layers Only, 0mm
    B66_L8_N, Signal Layers Only, 0mm
    B66_L8_P, Signal Layers Only, 0mm
    B66_L9_N, Signal Layers Only, 0mm
    B66_L9_P, Signal Layers Only, 0mm
    B66_T0, Signal Layers Only, 0mm
    B66_T1, Signal Layers Only, 0mm
    B66_T2, Signal Layers Only, 0mm
    B66_T3, Signal Layers Only, 0mm
    CAN_1_RX, Signal Layers Only, 0mm
    CAN_1_TX, Signal Layers Only, 0mm
    CAN_H, Signal Layers Only, 0mm
    CAN_H_0, Signal Layers Only, 0mm
    CAN_H_1, Signal Layers Only, 0mm
    CAN_H_2, Signal Layers Only, 0mm
    CAN_H_3, Signal Layers Only, 0mm
    CAN_L, Signal Layers Only, 0mm
    CAN_L_0, Signal Layers Only, 0mm
    CAN_L_1, Signal Layers Only, 0mm
    CAN_L_2, Signal Layers Only, 0mm
    CAN_L_3, Signal Layers Only, 0mm
    CAN_RX_LS, Signal Layers Only, 0mm
    CAN_TX_LS, Signal Layers Only, 0mm
    CLK0_N, Signal Layers Only, 0mm
    CLK0_P, Signal Layers Only, 0mm
    COM, Signal Layers Only, 0mm
    DONE, Signal Layers Only, 0mm
    DVDD1V, Signal Layers Only, 0mm
    EN_DDR, Signal Layers Only, 0mm
    EN_FPD, Signal Layers Only, 0mm
    EN_GT_L, Signal Layers Only, 0mm
    EN_GT_R, Signal Layers Only, 0mm
    EN_LPD, Signal Layers Only, 0mm
    EN_PL, Signal Layers Only, 0mm
    EN_PLL_PWR, Signal Layers Only, 0mm
    EN_PSGT, Signal Layers Only, 0mm
    ERR_OUT, Signal Layers Only, 0mm
    ERR_STATUS, Signal Layers Only, 0mm
    ETH_CLK, Signal Layers Only, 0mm
    ETH_MDC, Signal Layers Only, 0mm
    ETH_MDIO, Signal Layers Only, 0mm
    ETH_RST, Signal Layers Only, 0mm
    ETH_RXCK, Signal Layers Only, 0mm
    ETH_RXCTL, Signal Layers Only, 0mm
    ETH_RXD0, Signal Layers Only, 0mm
    ETH_RXD1, Signal Layers Only, 0mm
    ETH_RXD2, Signal Layers Only, 0mm
    ETH_RXD3, Signal Layers Only, 0mm
    ETH_TXCK, Signal Layers Only, 0mm
    ETH_TXCTL, Signal Layers Only, 0mm
    ETH_TXD0, Signal Layers Only, 0mm
    ETH_TXD1, Signal Layers Only, 0mm
    ETH_TXD2, Signal Layers Only, 0mm
    ETH_TXD3, Signal Layers Only, 0mm
    E_TCK, Signal Layers Only, 0mm
    E_TDI, Signal Layers Only, 0mm
    E_TDO, Signal Layers Only, 0mm
    E_TMS, Signal Layers Only, 0mm
    FGND, Signal Layers Only, 0mm
    GND, Signal Layers Only, 0mm
    GPIO_0, Signal Layers Only, 0mm
    GPIO_1, Signal Layers Only, 0mm
    GPIO_10, Signal Layers Only, 0mm
    GPIO_11, Signal Layers Only, 0mm
    GPIO_12, Signal Layers Only, 0mm
    GPIO_13, Signal Layers Only, 0mm
    GPIO_14, Signal Layers Only, 0mm
    GPIO_15, Signal Layers Only, 0mm
    GPIO_2, Signal Layers Only, 0mm
    GPIO_3, Signal Layers Only, 0mm
    GPIO_4, Signal Layers Only, 0mm
    GPIO_5, Signal Layers Only, 0mm
    GPIO_6, Signal Layers Only, 0mm
    GPIO_7, Signal Layers Only, 0mm
    GPIO_8, Signal Layers Only, 0mm
    GPIO_9, Signal Layers Only, 0mm
    GPIO_LED0, Signal Layers Only, 0mm
    GPIO_LED1, Signal Layers Only, 0mm
    GPIO_LED2, Signal Layers Only, 0mm
    GPIO_LED3, Signal Layers Only, 0mm
    GPIO_PB0, Signal Layers Only, 0mm
    GPIO_PB1, Signal Layers Only, 0mm
    GPIO_SW0, Signal Layers Only, 0mm
    GPIO_SW1, Signal Layers Only, 0mm
    GPIO_SW2, Signal Layers Only, 0mm
    GPIO_SW3, Signal Layers Only, 0mm
    GPIO_SW4, Signal Layers Only, 0mm
    GPIO_SW5, Signal Layers Only, 0mm
    GPIO_SW6, Signal Layers Only, 0mm
    GPIO_SW7, Signal Layers Only, 0mm
    HSx_TCK, Signal Layers Only, 0mm
    HSx_TDI, Signal Layers Only, 0mm
    HSx_TDO, Signal Layers Only, 0mm
    HSx_TMS, Signal Layers Only, 0mm
    I2C_0_SCL, Signal Layers Only, 0mm
    I2C_0_SDA, Signal Layers Only, 0mm
    INIT_B, Signal Layers Only, 0mm
    INPUT_GND, Signal Layers Only, 0mm
    LP_GOOD, Signal Layers Only, 0mm
    MOD_SENSE_N, Signal Layers Only, 0mm
    MOD_SENSE_P, Signal Layers Only, 0mm
    MR, Signal Layers Only, 0mm
    N_TCK, Signal Layers Only, 0mm
    N_TDI, Signal Layers Only, 0mm
    N_TDO, Signal Layers Only, 0mm
    N_TMS, Signal Layers Only, 0mm
    NetC101_1, Signal Layers Only, 0mm
    NetC102_2, Signal Layers Only, 0mm
    NetC103_2, Signal Layers Only, 0mm
    NetC12_1, Signal Layers Only, 0mm
    NetC19_1, Signal Layers Only, 0mm
    NetC19_2, Signal Layers Only, 0mm
    NetC1_1, Signal Layers Only, 0mm
    NetC37_2, Signal Layers Only, 0mm
    NetC44_1, Signal Layers Only, 0mm
    NetC45_2, Signal Layers Only, 0mm
    NetC52_1, Signal Layers Only, 0mm
    NetC60_1, Signal Layers Only, 0mm
    NetC62_2, Signal Layers Only, 0mm
    NetC65_2, Signal Layers Only, 0mm
    NetC66_1, Signal Layers Only, 0mm
    NetC67_2, Signal Layers Only, 0mm
    NetC93_1, Signal Layers Only, 0mm
    NetD10_1, Signal Layers Only, 0mm
    NetD10_2, Signal Layers Only, 0mm
    NetD11_1, Signal Layers Only, 0mm
    NetD11_2, Signal Layers Only, 0mm
    NetD12_1, Signal Layers Only, 0mm
    NetD12_2, Signal Layers Only, 0mm
    NetD13_1, Signal Layers Only, 0mm
    NetD13_2, Signal Layers Only, 0mm
    NetD14_2, Signal Layers Only, 0mm
    NetD15_2, Signal Layers Only, 0mm
    NetD2_2, Signal Layers Only, 0mm
    NetD3_2, Signal Layers Only, 0mm
    NetD4_2, Signal Layers Only, 0mm
    NetD6_1, Signal Layers Only, 0mm
    NetD6_2, Signal Layers Only, 0mm
    NetD7_2, Signal Layers Only, 0mm
    NetD8_1, Signal Layers Only, 0mm
    NetD9_1, Signal Layers Only, 0mm
    NetDS1_2, Signal Layers Only, 0mm
    NetDS2_2, Signal Layers Only, 0mm
    NetDS3_1, Signal Layers Only, 0mm
    NetF1_1, Signal Layers Only, 0mm
    NetF1_2, Signal Layers Only, 0mm
    NetJ26_2, Signal Layers Only, 0mm
    NetJ5_1, Signal Layers Only, 0mm
    NetP_4, Signal Layers Only, 0mm
    NetQ2_3, Signal Layers Only, 0mm
    NetQ3_3, Signal Layers Only, 0mm
    NetQ4_3, Signal Layers Only, 0mm
    NetQ5_3, Signal Layers Only, 0mm
    NetR103_2, Signal Layers Only, 0mm
    NetR104_2, Signal Layers Only, 0mm
    NetR105_2, Signal Layers Only, 0mm
    NetR106_2, Signal Layers Only, 0mm
    NetR13_2, Signal Layers Only, 0mm
    NetR14_2, Signal Layers Only, 0mm
    NetR15_2, Signal Layers Only, 0mm
    NetR16_2, Signal Layers Only, 0mm
    NetR18_1, Signal Layers Only, 0mm
    NetR1_2, Signal Layers Only, 0mm
    NetR20_2, Signal Layers Only, 0mm
    NetR22_1, Signal Layers Only, 0mm
    NetR25_1, Signal Layers Only, 0mm
    NetR28_2, Signal Layers Only, 0mm
    NetR30_1, Signal Layers Only, 0mm
    NetR5_1, Signal Layers Only, 0mm
    NetR67_1, Signal Layers Only, 0mm
    NetR68_V2, Signal Layers Only, 0mm
    NetR6_1, Signal Layers Only, 0mm
    NetR70_1, Signal Layers Only, 0mm
    NetR71_V2, Signal Layers Only, 0mm
    NetR73_1, Signal Layers Only, 0mm
    NetR74_V2, Signal Layers Only, 0mm
    NetR76_1, Signal Layers Only, 0mm
    NetR77_V2, Signal Layers Only, 0mm
    NetR95_1, Signal Layers Only, 0mm
    NetR97_2, Signal Layers Only, 0mm
    PCIE0_CLK_N, Signal Layers Only, 0mm
    PCIE0_CLK_P, Signal Layers Only, 0mm
    PCIE0_RST, Signal Layers Only, 0mm
    PCIE_RX0_N, Signal Layers Only, 0mm
    PCIE_RX0_P, Signal Layers Only, 0mm
    PCIE_RX1_N, Signal Layers Only, 0mm
    PCIE_RX1_P, Signal Layers Only, 0mm
    PCIE_RX2_N, Signal Layers Only, 0mm
    PCIE_RX2_P, Signal Layers Only, 0mm
    PCIE_RX3_N, Signal Layers Only, 0mm
    PCIE_RX3_P, Signal Layers Only, 0mm
    PCIE_TX0_N, Signal Layers Only, 0mm
    PCIE_TX0_P, Signal Layers Only, 0mm
    PCIE_TX1_N, Signal Layers Only, 0mm
    PCIE_TX1_P, Signal Layers Only, 0mm
    PCIE_TX2_N, Signal Layers Only, 0mm
    PCIE_TX2_P, Signal Layers Only, 0mm
    PCIE_TX3_N, Signal Layers Only, 0mm
    PCIE_TX3_P, Signal Layers Only, 0mm
    PER_EN, Signal Layers Only, 0mm
    PER_SENSE_N, Signal Layers Only, 0mm
    PER_SENSE_P, Signal Layers Only, 0mm
    PET0_N, Signal Layers Only, 0mm
    PET0_P, Signal Layers Only, 0mm
    PET1_N, Signal Layers Only, 0mm
    PET1_P, Signal Layers Only, 0mm
    PET2_N, Signal Layers Only, 0mm
    PET2_P, Signal Layers Only, 0mm
    PET3_N, Signal Layers Only, 0mm
    PET3_P, Signal Layers Only, 0mm
    PG_DDR, Signal Layers Only, 0mm
    PG_FPD, Signal Layers Only, 0mm
    PG_GT_L, Signal Layers Only, 0mm
    PG_GT_R, Signal Layers Only, 0mm
    PG_PL, Signal Layers Only, 0mm
    PG_PLL_1V8, Signal Layers Only, 0mm
    PG_PSGT, Signal Layers Only, 0mm
    PHY_LED0, Signal Layers Only, 0mm
    PHY_LED1, Signal Layers Only, 0mm
    PHY_LED2, Signal Layers Only, 0mm
    PHY_MDI0_N, Signal Layers Only, 0mm
    PHY_MDI0_P, Signal Layers Only, 0mm
    PHY_MDI1_N, Signal Layers Only, 0mm
    PHY_MDI1_P, Signal Layers Only, 0mm
    PHY_MDI2_N, Signal Layers Only, 0mm
    PHY_MDI2_P, Signal Layers Only, 0mm
    PHY_MDI3_N, Signal Layers Only, 0mm
    PHY_MDI3_P, Signal Layers Only, 0mm
    PLL_3V3, Signal Layers Only, 0mm
    PLL_LOLn, Signal Layers Only, 0mm
    PLL_RST, Signal Layers Only, 0mm
    PLL_SEL0, Signal Layers Only, 0mm
    PLL_SEL1, Signal Layers Only, 0mm
    PMBUS_SCL, Signal Layers Only, 0mm
    PMBUS_SDA, Signal Layers Only, 0mm
    PMOD0_0, Signal Layers Only, 0mm
    PMOD0_1, Signal Layers Only, 0mm
    PMOD0_2, Signal Layers Only, 0mm
    PMOD0_3, Signal Layers Only, 0mm
    PMOD1_0, Signal Layers Only, 0mm
    PMOD1_1, Signal Layers Only, 0mm
    PMOD1_2, Signal Layers Only, 0mm
    PMOD1_3, Signal Layers Only, 0mm
    PROG_B, Signal Layers Only, 0mm
    PRSNT, Signal Layers Only, 0mm
    PRSNT_X1, Signal Layers Only, 0mm
    PRSNT_X4, Signal Layers Only, 0mm
    PSBATT, Signal Layers Only, 0mm
    PS_MODE0, Signal Layers Only, 0mm
    PS_MODE1, Signal Layers Only, 0mm
    PS_MODE2, Signal Layers Only, 0mm
    PS_MODE3, Signal Layers Only, 0mm
    PUDC_B, Signal Layers Only, 0mm
    RXLED#, Signal Layers Only, 0mm
    Rfbb, Signal Layers Only, 0mm
    RxD_0, Signal Layers Only, 0mm
    RxD_1, Signal Layers Only, 0mm
    SD_CLK, Signal Layers Only, 0mm
    SD_CMD, Signal Layers Only, 0mm
    SD_DAT0, Signal Layers Only, 0mm
    SD_DAT1, Signal Layers Only, 0mm
    SD_DAT2, Signal Layers Only, 0mm
    SD_DAT3, Signal Layers Only, 0mm
    SD_DETECT, Signal Layers Only, 0mm
    SI_PLL_1V8, Signal Layers Only, 0mm
    SOM_TCK, Signal Layers Only, 0mm
    SOM_TDI, Signal Layers Only, 0mm
    SOM_TDO, Signal Layers Only, 0mm
    SOM_TMS, Signal Layers Only, 0mm
    SRST_B, Signal Layers Only, 0mm
    S_TCK, Signal Layers Only, 0mm
    S_TDI, Signal Layers Only, 0mm
    S_TDO, Signal Layers Only, 0mm
    S_TMS, Signal Layers Only, 0mm
    TCKO, Signal Layers Only, 0mm
    TDO_BI, Signal Layers Only, 0mm
    TDO_BO, Signal Layers Only, 0mm
    TMSO, Signal Layers Only, 0mm
    TXLED#, Signal Layers Only, 0mm
    TxD_0, Signal Layers Only, 0mm
    TxD_1, Signal Layers Only, 0mm
    UART_0_RXD, Signal Layers Only, 0mm
    UART_0_TXD, Signal Layers Only, 0mm
    UART_1_RXD, Signal Layers Only, 0mm
    UART_1_TXD, Signal Layers Only, 0mm
    USB_D_N, Signal Layers Only, 0mm
    USB_D_P, Signal Layers Only, 0mm
    USB_FILTR, Signal Layers Only, 0mm
    USB_GND, Signal Layers Only, 0mm
    USB_POWER_VIO, Signal Layers Only, 0mm
    VADJ, Signal Layers Only, 0mm
    VADJ_POK, Signal Layers Only, 0mm
    VBATT, Signal Layers Only, 0mm
    VCCO_PS, Signal Layers Only, 0mm
    VCC_USB, Signal Layers Only, 0mm
    W_TCK, Signal Layers Only, 0mm
    W_TDI, Signal Layers Only, 0mm
    W_TDO, Signal Layers Only, 0mm
    W_TMS, Signal Layers Only, 0mm
    eSD_CLK, Signal Layers Only, 0mm
    eSD_CMD, Signal Layers Only, 0mm
    eSD_DAT0, Signal Layers Only, 0mm
    eSD_DAT1, Signal Layers Only, 0mm
    eSD_DAT2, Signal Layers Only, 0mm
    eSD_DAT3, Signal Layers Only, 0mm
count : 578